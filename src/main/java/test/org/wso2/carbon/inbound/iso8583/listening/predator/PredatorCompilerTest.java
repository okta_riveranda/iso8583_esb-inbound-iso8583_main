package test.org.wso2.carbon.inbound.iso8583.listening.predator;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.gbg.cafs.predator.PredatorConstant;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.wso2.carbon.inbound.iso8583.listening.ISO8583Constant;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/** 
* PredatorCompiler Tester. 
* 
* @author <Authors name> 
* @since <pre>04/06/2021</pre> 
* @version 1.0 
*/ 
public class PredatorCompilerTest extends TestCase { 
public PredatorCompilerTest(String name) { 
super(name); 
} 

public void setUp() throws Exception { 
super.setUp(); 
} 

public void tearDown() throws Exception { 
super.tearDown(); 
} 

/** 
* 
* Method: XMLGetElementName(String XPath, String targetElem) 
* 
*/ 
public void testXMLGetElementName() throws Exception { 
//TODO: Test goes here...
    File inputFile = new File("C:\\wso2ei-7.1.0\\micro-integrator\\conf\\iso8583\\iso8583-configuration.xml");
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    Document doc = dBuilder.parse(inputFile);
    doc.getDocumentElement().normalize();
    NodeList nList = doc.getElementsByTagName(ISO8583Constant.ISO8583Request);
    String requestPath = nList.item(0).getTextContent();
    System.out.println("Request Path: "+requestPath);
} 

/** 
* 
* Method: XMLBodyComposer(String XPath, ISOMsg isomsg) 
* 
*/ 
public void testXMLBodyComposer() throws Exception { 
//TODO: Test goes here...
    String value = "5576922000000131";
    int maxIndex = value.length() - 1;
    int separatorIndex = value.indexOf("=");

    String alternativePAN = (separatorIndex == -1) ? value.substring(0, maxIndex) : value.substring(0, separatorIndex);
    String alternativeCardExp = (separatorIndex + 5 <= maxIndex && separatorIndex != -1) ? value.substring(separatorIndex+1, separatorIndex+5) : "";
    String serviceCode = (separatorIndex + 8 <= maxIndex && separatorIndex != -1) ? value.substring(separatorIndex+5, separatorIndex+8) : "";

    System.out.println("separatorIndex = "+separatorIndex);
    System.out.println("alternative PAN = "+alternativePAN);
    System.out.println("alternative CardExp = "+alternativeCardExp);
    System.out.println("serviceCode = "+serviceCode);
} 

/** 
* 
* Method: isFutureDate(String pDateString) 
* 
*/ 
public void testIsFutureDate() throws Exception { 
//TODO: Test goes here...

    Date date = null;
    try {
        date = new SimpleDateFormat("yyyyMMddHHmmss").parse("2021");
    } catch (ParseException e) {

    }
    System.out.println("date: "+date);
} 

/** 
* 
* Method: FormattertoValue(String type, String Formatter, String value) 
* 
*/ 
public void testFormattertoValue() throws Exception { 
//TODO: Test goes here... 
}

/** 
* 
* Method: ISOResponseComposer(ISOMsg isomsg) 
* 
*/ 
public void testISOResponseComposer() throws Exception { 
//TODO: Test goes here...

    Integer FieldNumber = null;
    File inputFile = new File("C:\\wso2ei-7.1.0\\micro-integrator\\conf\\iso8583\\iso8583-response.xml");
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    Document doc = dBuilder.parse(inputFile);
    doc.getDocumentElement().normalize();

    NodeList channels = doc.getElementsByTagName(ISO8583Constant.ISO8583ISOResponseTag);
    for (int i = 0; i < channels.getLength(); i++)
    {
        Node node = channels.item(i);
        if(Node.ELEMENT_NODE == node.getNodeType())
        {
            Element element = (Element) node;
            String mti = element.getAttribute("mti");

            if(ISO8583Constant.FINANCIAL_RESPONSE.equals(mti))
            {
                NodeList financialResponse = element.getChildNodes();
                for(int j = 0; j < financialResponse.getLength(); j++)
                {
                    Node responseNode = financialResponse.item(j);
                    if(Node.ELEMENT_NODE == responseNode.getNodeType())
                    {
                        Element financialResponseElement = (Element) responseNode;
                        String attr = "";
                        String defValue = "";
                        FieldNumber = Integer.valueOf(financialResponseElement.getTagName().replaceAll("field", ""));
                        if (!financialResponseElement.getAttribute("attr").equals(""))
                        {
                            attr = financialResponseElement.getAttribute("attr");
                        }
                        if ("ME".equals(attr)) {
                            System.out.println("ME: "+FieldNumber);
                        }
                        else if ("CE".equals(attr)) {
                            System.out.println("CE: "+FieldNumber);
                        }
                    }
                }
            }
        }
    }


} 

/** 
* 
* Method: ConfigReader(ISOMsg isomsg) 
* 
*/ 
public void testConfigReader() throws Exception { 
//TODO: Test goes here... 
} 


/** 
* 
* Method: convertDocumentToString(Document doc) 
* 
*/ 
public void testConvertDocumentToString() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = PredatorCompiler.getClass().getMethod("convertDocumentToString", Document.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/

    Integer FieldNumber = null;
    File inputFile = new File("C:\\wso2ei-7.1.0\\micro-integrator\\conf\\iso8583\\iso8583-request.xml");
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    Document doc = dBuilder.parse(inputFile);
    doc.getDocumentElement().normalize();

    NodeList channels = doc.getElementsByTagName(ISO8583Constant.ISO8583ISORequestTag);
    for (int i = 0; i < channels.getLength(); i++)
    {
        Node node = channels.item(i);
        if(Node.ELEMENT_NODE == node.getNodeType())
        {
            Element element = (Element) node;
            String channel = element.getAttribute("channel");

            if(PredatorConstant.PREDATOR_CHANNEL_ONLINE.equals(channel))
            {
                NodeList financialRequest = element.getChildNodes();
                Node requestNode = financialRequest.item(1);

                NodeList requestNodeChildren = requestNode.getChildNodes();
                System.out.println("requestNodeChildren.getLength() = "+requestNodeChildren.getLength());
                for(int j = 0; j < requestNodeChildren.getLength(); j++)
                {
                    Node requestNodeChild = requestNodeChildren.item(j);
                    if(Node.ELEMENT_NODE == requestNodeChild.getNodeType())
                    {
                        Element financialRequestElement = (Element) requestNodeChild;
                        String attr = "";
                        String defValue = "";
                        System.out.println("Field Name: "+financialRequestElement.getTagName());
                    }
                }
            }
        }
    }

} 

/** 
* 
* Method: handleException(String msg, Exception e) 
* 
*/ 
public void testHandleException() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = PredatorCompiler.getClass().getMethod("handleException", String.class, Exception.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 


public static Test suite() { 
return new TestSuite(PredatorCompilerTest.class); 
} 
} 
