package test.org.wso2.carbon.inbound.iso8583.listening.predator;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/** 
* PredatorSendBack Tester. 
* 
* @author <Authors name> 
* @since <pre>04/06/2021</pre> 
* @version 1.0 
*/ 
public class PredatorSendBackTest extends TestCase {
public PredatorSendBackTest(String name) {
super(name); 
} 

public void setUp() throws Exception { 
super.setUp(); 
} 

public void tearDown() throws Exception { 
super.tearDown(); 
} 

/** 
* 
* Method: getCloseableHttpClient() 
* 
*/ 
public void testGetCloseableHttpClient() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: sendBack(ISOMsg isomsg) 
* 
*/ 
public void testSendBack() throws Exception { 
//TODO: Test goes here...
} 


/** 
* 
* Method: handleException(Exception e) 
* 
*/ 
public void testHandleException() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = PredatorSendBack.getClass().getMethod("handleException", Exception.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 


public static Test suite() { 
return new TestSuite(PredatorSendBackTest.class); 
} 
} 
