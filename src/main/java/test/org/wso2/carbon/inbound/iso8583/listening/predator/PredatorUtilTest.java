package test.org.wso2.carbon.inbound.iso8583.listening.predator;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.gbg.cafs.predator.PredatorUtil;

import java.util.Hashtable;

/** 
* PredatorUtil Tester. 
* 
* @author <Authors name> 
* @since <pre>04/03/2021</pre> 
* @version 1.0 
*/ 
public class PredatorUtilTest extends TestCase { 
public PredatorUtilTest(String name) { 
super(name); 
} 

public void setUp() throws Exception { 
super.setUp(); 
} 

public void tearDown() throws Exception { 
super.tearDown(); 
} 

/** 
* 
* Method: getXMLResponseField(String fieldName) 
* 
*/ 
public void testGetXMLResponseField() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getResponseCode(boolean alert, String ruleDecision) 
* 
*/ 
public void testGetResponseCode() throws Exception { 
//TODO: Test goes here... 
} 

/**
* 
* Method: initiateTLVExtraction(String tlvMessage) 
* 
*/ 
public void testInitiateTLVExtraction() throws Exception { 
//TODO: Test goes here...
    PredatorUtil predatorUtil = new PredatorUtil();
    Hashtable extractedTLV = predatorUtil.initiateTLVExtraction("0103PRM0301N0901N1007JAKARTA1102ID");
    extractedTLV.forEach(
            (k,v) -> System.out.println("Key: "+k+" and Value: "+v)
    );
    System.out.println("What is 01 = "+((extractedTLV == null)? "" : (String)extractedTLV.get("01")));

} 

/** 
* 
* Method: extractTLV(ArrayList list) 
* 
*/ 
public void testExtractTLV() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getTLVFieldName(String fieldNo) 
* 
*/ 
public void testGetTLVFieldName() throws Exception { 
//TODO: Test goes here... 
} 



public static Test suite() { 
return new TestSuite(PredatorUtilTest.class); 
} 
} 
