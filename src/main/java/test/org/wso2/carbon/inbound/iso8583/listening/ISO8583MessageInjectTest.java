package test.org.wso2.carbon.inbound.iso8583.listening;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.jpos.iso.ISOMsg;

/** 
* ISO8583MessageInject Tester. 
* 
* @author <Authors name> 
* @since <pre>03/23/2021</pre> 
* @version 1.0 
*/ 
public class ISO8583MessageInjectTest extends TestCase { 
public ISO8583MessageInjectTest(String name) { 
super(name); 
} 

public void setUp() throws Exception { 
super.setUp(); 
} 

public void tearDown() throws Exception { 
super.tearDown(); 
} 

/** 
* 
* Method: inject(ISOMsg isoMessageReq) 
* 
*/ 
public void testInject() throws Exception { 
//TODO: Test goes here...
    ISOMsg msg = new ISOMsg("02360200F23C440128A1800000000000040000001652110220000000738401  0000050000000323093001297168101343021099126011000021400449234      G9985580BANK CENTRAL ASIA     JAKARTA         ID0340103PRM0301N0901N1007JAKARTA1102ID360190000000800136839900");

} 


/** 
* 
* Method: createMessageContext(ISOMsg isoMessageReq) 
* 
*/ 
public void testCreateMessageContext() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = ISO8583MessageInject.getClass().getMethod("createMessageContext", ISOMsg.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: messageBuilder(ISOMsg isomsg) 
* 
*/ 
public void testMessageBuilder() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = ISO8583MessageInject.getClass().getMethod("messageBuilder", ISOMsg.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 


public static Test suite() { 
return new TestSuite(ISO8583MessageInjectTest.class); 
} 
} 
