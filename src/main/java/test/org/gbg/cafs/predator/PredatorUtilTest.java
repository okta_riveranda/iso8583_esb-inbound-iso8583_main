package test.org.gbg.cafs.predator; 

import junit.framework.Test; 
import junit.framework.TestSuite; 
import junit.framework.TestCase;
import org.gbg.cafs.predator.PredatorConstant;
import org.gbg.cafs.predator.PredatorUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** 
* PredatorUtil Tester. 
* 
* @author <Authors name> 
* @since <pre>04/12/2021</pre> 
* @version 1.0 
*/ 
public class PredatorUtilTest extends TestCase { 
public PredatorUtilTest(String name) { 
super(name); 
} 

public void setUp() throws Exception { 
super.setUp(); 
} 

public void tearDown() throws Exception { 
super.tearDown(); 
} 

/** 
* 
* Method: getXMLResponseField(String fieldName) 
* 
*/ 
public void testGetXMLResponseField() throws Exception { 
//TODO: Test goes here...
    boolean isMultipleISOMessage = false;
    String request = "03140200F63E440128E1A0000000000002000000165327130000005694IW02  000000000000000000000000060310583941068910581606039998060360110510222375327130000005694=240822600000061200003059        Z009    22             UAT BINTARO GN2 BASE ENBANTEN TANGSEIDID0410103ATM0301N0901Y1013NBANTEN TANGS1103DID360   19000000070307434490002930200F63E440128E1A0000000000000000000165327130000005694IW02  000000000000000000000000060311002541202411002506032408060360110510222375327130000005694=240822600000061200003061        Z009    22             UAT BINTARO GN2 BASE ENBANTEN TANGSEIDID0410103ATM0301N0901Y1013NBANTEN TANGS1103DID360   03140200F63E440128E1A0000000000002000000165327130000005694IW02  000000000000000000000000060311003441202411002506039998060360110510222375327130000005694=240822600000061200003061        Z009    22             UAT BINTARO GN2 BASE ENBANTEN TANGSEIDID0410103ATM0301N0901Y1013NBANTEN TANGS1103DID360   19000000070307434490002930200F63E440128E1A0000000000000000000165327130000005694IW02  000000000000000000000000060311011941244011011906032408060360110510222375327130000005694=240822600000061200003062        Z009    22             UAT BINTARO GN2 BASE ENBANTEN TANGSEIDID0410103ATM0301N0901Y1013NBANTEN TANGS1103DID360   03140200F63E440128E1A0000000000002000000165327130000005694IW02  000000000000000000000000060311012641244011011906039998060360110510222375327130000005694=240822600000061200003062        Z009    22             UAT BINTARO GN2 BASE ENBANTEN TANGSEIDID0410103ATM0301N0901Y1013NBANTEN TANGS1103DID360   19000000070307434490002930200F63E440128E1A0000000000000000000165327130000005694IW02  000000000000000000000000060311015641269911015606032408060360110510222375327130000005694=240822600000061200003063        Z009    22             UAT BINTARO GN2 BASE ENBANTEN TANGSEIDID0410103ATM0301N0901Y1013NBANTEN TANGS1103DID360   03140200F63E440128E1A0000000000002000000165327130000005694IW02  000000000000000000000000060311020941269911015606039998060360110510222375327130000005694=240822600000061200003063        Z009    22             UAT BINTARO GN2 BASE ENBANTEN TANGSEIDID0410103ATM0301N0901Y1013NBANTEN TANGS1103DID360   19000000070307434490002930200F63E440128E1A0000000000000000000165327130000005694IW02  000000000000000000000000060311023041303911023006032408060360110510222375327130000005694=240822600000061200003064        Z009    22             UAT BINTARO GN2 BASE ENBANTEN TANGSEIDID0410103ATM0301N0901Y1013NBANTEN TANGS1103DID360   03140200F63E440128E1A0000000000002000000165327130000005694IW02  000000000000000000000000060311023741303911023006039998060360110510222375327130000005694=240822600000061200003064        Z009    22             UAT BINTARO GN2 BASE ENBANTEN TANGSEIDID0410103ATM0301N0901Y1013NBANTEN TANGS1103DID360   190000000703074344900";
    // request = "03140200F63E440128E1A0000000000002000000165327130000005694IW02  000000000000000000000000060310583941068910581606039998060360110510222375327130000005694=240822600000061200003059        Z009    22             UAT BINTARO GN2 BASE ENBANTEN TANGSEIDID0410103ATM0301N0901Y1013NBANTEN TANGS1103DID360   190000000703074344900";
    int maxHeaderLength = 4;
    int firstHeaderLength = Integer.parseInt(request.substring(0,maxHeaderLength));
    int requestLength = request.substring(5,request.length()).length();

    isMultipleISOMessage = (firstHeaderLength < requestLength) ? true:false;
    if(isMultipleISOMessage)
    {
        ArrayList<String> isoRequestList = new ArrayList();
        System.out.println("Multiple ISO!");
        int counter = 1;
        int headerLength = firstHeaderLength;
        int firstIndex = 0;
        while(request.length() > 0)
        {
            // System.out.println("Full Request: "+request);
            String tempIsoRequest = request.substring(firstIndex + maxHeaderLength, firstHeaderLength+maxHeaderLength);
            System.out.println("ISO Message "+counter+": "+tempIsoRequest);
            isoRequestList.add(tempIsoRequest);
            request = request.substring(firstHeaderLength+maxHeaderLength);
            // System.out.println("Full Request after changed: "+request);
            if(request.isEmpty())
            {
                break;
            }
            firstHeaderLength = Integer.parseInt(request.substring(firstIndex, maxHeaderLength));
            counter++;
        }
    }
    else
    {
        System.out.println("Single ISO!");
    }
} 

/** 
* 
* Method: getResponseCode(boolean alert, String ruleDecision) 
* 
*/ 
public void testGetResponseCode() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: initiateTLVExtraction(String tlvMessage) 
* 
*/ 
public void testInitiateTLVExtraction() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: extractTLV(ArrayList list) 
* 
*/ 
public void testExtractTLV() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: extractTrack2DataB(String track2Data, String fieldName) 
* 
*/ 
public void testExtractTrack2DataB() throws Exception { 
//TODO: Test goes here...

    String message = "5576922000000131";
    System.out.println("PAN: "+PredatorUtil.extractTrack2DataB(message, PredatorConstant.BIT_TRACK2_DATA_MULTIPLE_FIELDS[0]));
    System.out.println("CardExp: "+PredatorUtil.extractTrack2DataB(message, PredatorConstant.BIT_TRACK2_DATA_MULTIPLE_FIELDS[1]));
    System.out.println("Service Code: "+PredatorUtil.extractTrack2DataB(message, PredatorConstant.BIT_TRACK2_DATA_MULTIPLE_FIELDS[2]));

} 



public static Test suite() { 
return new TestSuite(PredatorUtilTest.class); 
} 
} 
