package org.wso2.carbon.inbound.iso8583.listening;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.synapse.SynapseException;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Hashtable;

public class ISO8583CustomConverter {
    private static final Logger log = Logger.getLogger(ISO8583CustomConverter.class);
    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static String hexToAscii(String hexStr) {
        StringBuilder output = new StringBuilder();
        try {
            if (hexStr.length() % 2 == 1) {
                hexStr = hexStr + " ";
            }
            for (int i = 0; i < hexStr.length(); i += 2) {
                String str = hexStr.substring(i, i + 2);
                output.append((char) Integer.parseInt(str, 16));
            }

        } catch (Exception e) {
            handleException("hexToAscii  Error", e);
        }
        return output.toString();
    }

    public static String AsciiToHex(String str) {
        char[] chars = str.toCharArray();
        StringBuilder hex = new StringBuilder();
        try {
            for (char aChar : chars) {
                hex.append(Integer.toHexString(aChar));
            }
        } catch (Exception e) {
            handleException("AsciiToHex  Error", e);
        }

        return hex.toString();
    }

    public static String CustomVarLEncode(int num, int digits) {
        String Hex = "";
        try {
            Hex = Integer.toHexString(num);
        } catch (Exception e) {
            handleException("CustomVarLEncode  Error", e);
        }
        return StringUtils.leftPad(Hex, digits, "0");
    }

    public static String CustomVarLDecode(byte[] b, int offset, int nDigits) {
        String Hex = "";
        try {
            for (int i = 0; i < nDigits; i++) {
                int iX = b[offset + i] & 0xFF;
                Hex = Hex.concat(ISO8583CustomConverter.hexToAscii(Integer.toHexString(iX)));
            }
        } catch (Exception e) {
            handleException("CustomVarLDecode  Error", e);
        }
        return Hex;
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static byte[] hexStringToByteArray(String s) {
        if (s.length() % 2 == 1)
            s = s + " ";
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static byte[] WrapMessage(int spacerByte, byte[] message) {

        // Get the length prefix for the message

        //int AsciiMsgLength = new String(message, Charset.forName("US-ASCII")).length()+ spacerByte;
        // String pad = Character.toString((char)0);
        //String LengthChar="";
        byte[] buffer = new byte[spacerByte + message.length];

        byte[] padByte = new byte[spacerByte];
        for (int i = 0; i < spacerByte; i++) {
            padByte[i] = 0;
        }
        // LengthChar = StringUtils.leftPad(new String(message, Charset.forName("US-ASCII")), AsciiMsgLength, pad);
        //message=LengthChar.getBytes(Charset.forName("US-ASCII"));
        System.arraycopy(padByte, 0, buffer, 0, padByte.length);
        System.arraycopy(message, 0, buffer, padByte.length, message.length);

        byte[] lengthPrefix = new byte[4];

        try {
            lengthPrefix = ISO8583CustomConverter.GetMsgLengthByte(buffer.length);
        } catch (IOException e) {
            handleException("GetMsgLengthByte  Error", e);
        }

        // Concatenate the length prefix and the message
        byte[] headerLength = String.format("%04d", buffer.length).getBytes();
//        log.info("lengthPrefix.length = "+headerLength.length);
//        log.info("lengthPrefix = "+(new String(headerLength, ISO8583Constant.ASCII_METHOD)));

//        log.info("buffer.length = "+buffer.length);
//        log.info("buffer = "+(new String(buffer, ISO8583Constant.ASCII_METHOD)));
        byte[] ret = new byte[headerLength.length + buffer.length];

        System.arraycopy(headerLength, 0, ret, 0, headerLength.length);
        System.arraycopy(buffer, 0, ret, headerLength.length, buffer.length);

        return ret;
    }

    private static void handleException(String msg, Exception e) {
        log.error(msg, e);
        throw new SynapseException(msg, e);
    }

    public static byte[] GetMsgLengthByte(int v) throws IOException {

        byte[] ret = new byte[4];
        ret[0] = (byte) ((v >>> 24) & 0xFF);
        ret[1] = (byte) ((v >>> 16) & 0xFF);
        ret[2] = (byte) ((v >>> 8) & 0xFF);
        ret[3] = (byte) ((v >>> 8) & 0xFF);
        return ret;

    }

    // this method is to truncate n digit from the ISO message before unpack
    public static byte[] removeHeaderLength(byte[] message, int n) {
        if (message != null) {
            String strMessage = new String(message, StandardCharsets.UTF_8);
            strMessage.substring(n);
            return strMessage.getBytes();
        }
        return null;
    }

    public static String FormatCurrency(String cur, String value) {

        Hashtable<Integer, Integer> myCurrency = new Hashtable<Integer, Integer>();
        myCurrency.put(48, 3);
        myCurrency.put(108, 0);
        myCurrency.put(974, 0);
        myCurrency.put(990, 0);
        myCurrency.put(152, 0);
        myCurrency.put(132, 0);
        myCurrency.put(262, 0);
        myCurrency.put(324, 0);
        myCurrency.put(368, 3);
        myCurrency.put(364, 0);
        myCurrency.put(352, 0);
        myCurrency.put(400, 3);
        myCurrency.put(392, 0);
        myCurrency.put(174, 0);
        myCurrency.put(408, 0);
        myCurrency.put(410, 0);
        myCurrency.put(414, 3);
        myCurrency.put(418, 0);
        myCurrency.put(422, 0);
        myCurrency.put(434, 3);
        myCurrency.put(969, 1);
        myCurrency.put(807, 0);
        myCurrency.put(104, 0);
        myCurrency.put(478, 1);
        myCurrency.put(512, 3);
        myCurrency.put(600, 0);
        myCurrency.put(646, 0);
        myCurrency.put(694, 0);
        myCurrency.put(678, 0);
        myCurrency.put(788, 3);
        myCurrency.put(940, 0);
        myCurrency.put(704, 0);
        myCurrency.put(548, 0);
        myCurrency.put(950, 0);
        myCurrency.put(961, 0);
        myCurrency.put(959, 0);
        myCurrency.put(955, 0);
        myCurrency.put(956, 0);
        myCurrency.put(957, 0);
        myCurrency.put(958, 0);
        myCurrency.put(960, 0);
        myCurrency.put(000, 0);
        myCurrency.put(952, 0);
        myCurrency.put(964, 0);
        myCurrency.put(953, 0);
        myCurrency.put(962, 0);
        myCurrency.put(963, 0);
        myCurrency.put(999, 0);

        int icurrency = Integer.parseInt(cur);
        int conversion = 2;
        if (myCurrency.containsKey(icurrency)) {
            conversion = myCurrency.get(icurrency);
        }
        long integral2 = Long.parseLong(value);
        int counter2 = conversion;
        BigDecimal unscaled2 = new BigDecimal(integral2);
        BigDecimal scaled2 = unscaled2.scaleByPowerOfTen(-counter2);
        value = String.valueOf(scaled2);

        return value;
    }

    public static ArrayList<String> checkAndExtractMultipleISO(byte[] message)
    {
        ArrayList<String> isoRequestList = new ArrayList();
        boolean isMultipleISOMessage = false;
        String request = new String(message, ISO8583Constant.ASCII_METHOD);

        int maxHeaderLength = 4;
        int firstHeaderLength = Integer.parseInt(request.substring(0,maxHeaderLength));
        int requestLength = request.substring(5,request.length()).length();

        int headerLength = firstHeaderLength;
        int firstIndex = 0;

        isMultipleISOMessage = (firstHeaderLength < requestLength) ? true:false;
        if(isMultipleISOMessage)
        {
            int counter = 1;
            while(request.length() > 0)
            {
                String tempIsoRequest = request.substring(firstIndex + maxHeaderLength, firstHeaderLength+maxHeaderLength);
                isoRequestList.add(tempIsoRequest);
                request = request.substring(firstHeaderLength+maxHeaderLength);
                if(request.isEmpty())
                {
                    break;
                }
                firstHeaderLength = Integer.parseInt(request.substring(firstIndex, maxHeaderLength));
                counter++;
            }
        }
        else
        {
            String tempIsoRequest = request.substring(firstIndex + maxHeaderLength, firstHeaderLength+maxHeaderLength);
            isoRequestList.add(tempIsoRequest);
        }
        return isoRequestList;
    }

}