/*
 * Copyright (c) 2016, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.wso2.carbon.inbound.iso8583.listening;

import java.nio.charset.Charset;

/**
 * Class ISO8583Constant defines all constants used for ISO8583 inbound.
 */
public class ISO8583Constant {
    public final static String HEADER = "header";
    public final static String TAG_FIELD = "field";
    public final static String TAG_MSG = "ISOMessage";
    public final static String TAG_DATA = "data";
    public final static String TAG_ID = "id";
    public static final String PORT = "port";
    public static final String ISO8583_1993_PACKAGER= "jposdef_1993.xml";
    public static final String ISO8583_1987_PACKAGER = "jposdef_1987.xml";
    public static final int ISO8583_1993_HEADER_LENGTH = 42;
    public static final int ISO8583_1987_HEADER_LENGTH = 4;
    public static final String CORE_THREADS = "16";
    public static final String MAX_THREADS = "16";
    public static final String KEEP_ALIVE = "10";
    public static final String THREAD_QLEN = "999";
    public static final String INBOUND_CORE_THREADS = "coreThreads";
    public static final String ISO8583_CONFIGURATION_PATH = "ISO8583ConfigPath";
    public static final String INBOUND_MAX_THREADS = "maxThreads";
    public static final String INBOUND_THREAD_ALIVE = "keepAlive";
    public static final String INBOUND_THREAD_QLEN = "queueLength";
    public static final String INBOUND_SEQUENTIAL = "sequential";
    public static final String INBOUND_HEADER_LENGTH = "headerLength";
    public static final String INBOUND_ACT_AS_PROXY = "isProxy";
    public final static String ISO8583_INBOUND_MSG_ID = "ISO8583_INBOUND_MSG_ID";
    public static final String FINANCIAL_REQUEST = "0200";
    public static final String FINANCIAL_RESPONSE = "0210";
    public static final String AUTHORIZATION_REQUEST = "0100";
    public static final String AUTHORIZATION_RESPONSE = "0110";
    public static final String NETWORK_MANAGEMENT_REQUEST = "0800";
    public static final String NETWORK_MANAGEMENT_RESPONSE = "0810";
    public static final String ISO8583_1987_VERSION = "1987";
    public static final String ISO8583_1993_VERSION = "1993";
    public static final String[] PROCESS_CODES = {"99", "999"};
    public static final Charset ASCII_METHOD = Charset.forName("US-ASCII");
    public static final int RESPONSE_CODE_FIELD_NUMBER = 39;
    public static final int TLV = 999;
    public static final int TLVFieldNumber = 48;
    public static String ISO8583Request = "ISO8583Request";
    public static String ISO8583Response = "ISO8583Response";
    public static String ISO8583ISORequestTag = "ISORequest";
    public static String ISO8583ISOResponseTag = "ISOResponse";
    public static String ReplySenderTag = "MTIReplySender";
}
