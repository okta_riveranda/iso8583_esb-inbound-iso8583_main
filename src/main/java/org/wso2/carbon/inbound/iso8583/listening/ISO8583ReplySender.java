/*
 * Copyright (c) 2016, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.wso2.carbon.inbound.iso8583.listening;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.soap.SOAPEnvelope;
import org.apache.log4j.Logger;
import org.apache.synapse.MessageContext;
import org.apache.synapse.SynapseException;
import org.apache.synapse.inbound.InboundProcessorParams;
import org.apache.synapse.inbound.InboundResponseSender;
import org.jpos.iso.ISOBasePackager;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import javax.xml.namespace.QName;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Base64;
import java.util.Iterator;
import java.util.Properties;

/**
 * class for handle iso8583 responses.
 */
public class ISO8583ReplySender implements InboundResponseSender {
    private static final Logger log = Logger.getLogger(ISO8583ReplySender.class.getName());
    private final Socket connection;
    private final InboundProcessorParams params;
    private int spacerByte;
    private String sPackager;
    /**
     * keep the socket connection to send the response back to client.
     *
     * @param connection created socket connection.
     */
    public ISO8583ReplySender(Socket connection, InboundProcessorParams params) {
        this.connection = connection;
        this.params = params;
    }


    /**
     * Send the reply or response back to the client.
     *
     * @param messageContext to get xml iso message from message context.
     */
    public void sendBack(MessageContext messageContext, boolean isMany) {
        byte[] responseMessage = null;
        String messageDumpHex=null;
        String messageDumpASCII=null;
        try {
            Properties properties = params.getProperties();
            this.sPackager= properties.getProperty("packager");
            this.spacerByte= Integer.valueOf(properties.getProperty("spacerByte"));
            ISOBasePackager packager = ISO8583PackagerFactory.getPackager(this.sPackager,params);

            //Retrieve the SOAP envelope from the MessageContext
            SOAPEnvelope soapEnvelope = messageContext.getEnvelope();
            OMElement getElements = soapEnvelope.getBody().getFirstElement();
            if (getElements == null) {
                handleException("Failed to get response message", null);
            }
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            if (packager.getHeaderLength() > 0) {
                String header = getElements.getFirstChildWithName(new QName(ISO8583Constant.HEADER)).getText();
                isoMsg.setHeader(Base64.getDecoder().decode(header));
            }
            Iterator fields = getElements.getFirstChildWithName(
                    new QName(ISO8583Constant.TAG_DATA)).getChildrenWithLocalName(ISO8583Constant.TAG_FIELD);
            while (fields.hasNext()) {
                OMElement element = (OMElement) fields.next();
                String getValue = element.getText();
                try {
                    int fieldID = Integer.parseInt(element.getAttribute(
                            new QName(ISO8583Constant.TAG_ID)).getAttributeValue());
                    isoMsg.set(fieldID, getValue);
                } catch (NumberFormatException e) {
                    log.warn("The fieldID does not contain a parsable integer" + e.getMessage(), e);
                }
            }

            /* isProxy defines whether this inbound is acting as a proxy for
                another backend service or processing the message itself.
                if the inbound endpoint act as a proxy to another service
                pack the ISO message without change any field
             */
            if (isProxy()) {
                responseMessage = isoMsg.pack();
            } else {
                isoMsg.recalcBitMap();
                responseMessage = isoMsg.pack();

                if (ISO8583Constant.ISO8583_1993_VERSION.equals(sPackager)) {
                    messageDumpHex= new String(responseMessage, ISO8583Constant.ASCII_METHOD);
                    responseMessage = ISO8583CustomConverter.hexStringToByteArray(messageDumpHex);
                    messageDumpASCII= new String(responseMessage, ISO8583Constant.ASCII_METHOD);
                } else {
                    messageDumpASCII= new String(responseMessage,  ISO8583Constant.ASCII_METHOD);
                    messageDumpHex= new String(ISO8583CustomConverter.bytesToHex(responseMessage).getBytes( ISO8583Constant.ASCII_METHOD),  ISO8583Constant.ASCII_METHOD);
                }
            }
        } catch (ISOException e) {
            handleException("Couldn't pack ISO8583 Messages - ", e);
        }

        sendResponse(responseMessage, isMany);
//        log.debug("[HEX] ISO Response Raw: "+ messageDumpHex);
//        log.debug("[ASCII] ISO Response Raw: " + messageDumpASCII);
        log.debug("ISO Response sent!");
    }

    public void sendBack(MessageContext[] messagesContext)
    {
        byte[][] responseMessage = null;
        String messageDumpHex=null;
        String messageDumpASCII=null;
        try {
            Properties properties = params.getProperties();
            this.sPackager= properties.getProperty("packager");
            this.spacerByte= Integer.valueOf(properties.getProperty("spacerByte"));
            ISOBasePackager packager = ISO8583PackagerFactory.getPackager(this.sPackager,params);

            //Retrieve the SOAP envelope from the MessageContext
            for(int i = 0; i < messagesContext.length; i++)
            {
                SOAPEnvelope soapEnvelope = messagesContext[i].getEnvelope();
                OMElement getElements = soapEnvelope.getBody().getFirstElement();
                if (getElements == null) {
                    handleException("Failed to get response message", null);
                }
                ISOMsg isoMsg = new ISOMsg();
                isoMsg.setPackager(packager);
                if (packager.getHeaderLength() > 0) {
                    String header = getElements.getFirstChildWithName(new QName(ISO8583Constant.HEADER)).getText();
                    isoMsg.setHeader(Base64.getDecoder().decode(header));
                }
                Iterator fields = getElements.getFirstChildWithName(
                        new QName(ISO8583Constant.TAG_DATA)).getChildrenWithLocalName(ISO8583Constant.TAG_FIELD);
                while (fields.hasNext()) {
                    OMElement element = (OMElement) fields.next();
                    String getValue = element.getText();
                    try {
                        int fieldID = Integer.parseInt(element.getAttribute(
                                new QName(ISO8583Constant.TAG_ID)).getAttributeValue());
                        isoMsg.set(fieldID, getValue);
                    } catch (NumberFormatException e) {
                        log.warn("The fieldID does not contain a parsable integer" + e.getMessage(), e);
                    }
                }

            /* isProxy defines whether this inbound is acting as a proxy for
                another backend service or processing the message itself.
                if the inbound endpoint act as a proxy to another service
                pack the ISO message without change any field
             */
                if (isProxy()) {
                    responseMessage[i] = isoMsg.pack();
                } else {
                    isoMsg.recalcBitMap();
                    responseMessage[i] = isoMsg.pack();

                    if (ISO8583Constant.ISO8583_1993_VERSION.equals(sPackager)) {
                        messageDumpHex= new String(responseMessage[i], ISO8583Constant.ASCII_METHOD);
                        responseMessage[i] = ISO8583CustomConverter.hexStringToByteArray(messageDumpHex);
                        messageDumpASCII= new String(responseMessage[i], ISO8583Constant.ASCII_METHOD);
                    } else {
                        messageDumpASCII= new String(responseMessage[i],  ISO8583Constant.ASCII_METHOD);
                        messageDumpHex= new String(ISO8583CustomConverter.bytesToHex(responseMessage[i]).getBytes( ISO8583Constant.ASCII_METHOD),  ISO8583Constant.ASCII_METHOD);
                    }
                }
            }
        } catch (ISOException e) {
            handleException("Couldn't pack ISO8583 Messages - ", e);
        }

        sendResponse(responseMessage);
        log.debug("ISO Response sent!");
    }


    /**
     * writes the packed iso message response to the client.
     *
     * @param responseMessage byte of packed ISO response.
     */
    private void sendResponse(byte[] responseMessage, boolean isMany) {
        try {
            DataOutputStream outToClient = new DataOutputStream(connection.getOutputStream());
            responseMessage= ISO8583CustomConverter.WrapMessage(this.spacerByte,responseMessage);
            log.debug("ISO Message Response to Client: " + new String(responseMessage, ISO8583Constant.ASCII_METHOD));
            if(!isMany)
            {
                outToClient.write(responseMessage);
                outToClient.flush();
            }
        } catch (IOException e) {
            handleException("OutputStream may be closed ", e);
        }
    }

    private void sendResponse(byte[][] responseMessage) {
        try {
            DataOutputStream outToClient = new DataOutputStream(connection.getOutputStream());
            for(int i = 0; i < responseMessage.length; i++)
            {
                responseMessage[i] = ISO8583CustomConverter.WrapMessage(this.spacerByte,responseMessage[i]);
                log.debug("ISO Message Response to Client: " + new String(responseMessage[i], ISO8583Constant.ASCII_METHOD));
                outToClient.write(responseMessage[0]);
                outToClient.flush();
            }
        } catch (IOException e) {
            handleException("OutputStream may be closed ", e);
        }
    }


    /**
     * handle the Exception
     *
     * @param msg error message
     * @param e   an Exception
     */
    private void handleException(String msg, Exception e) {
        log.error(msg, e);
        throw new SynapseException(msg);
    }

    /**
     * Is this inbound endpoint act as a proxy to another service
     *
     * @return act as a proxy or not
     */
    private boolean isProxy() {
        Properties properties = this.params.getProperties();
        return Boolean.parseBoolean(properties.getProperty(ISO8583Constant.INBOUND_ACT_AS_PROXY));
    }

    @Override
    public void sendBack(MessageContext messageContext) {

    }
}
