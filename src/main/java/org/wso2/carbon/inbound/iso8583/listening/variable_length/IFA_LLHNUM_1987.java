
package org.wso2.carbon.inbound.iso8583.listening.variable_length;

import org.wso2.carbon.inbound.iso8583.listening.ISO8583CustomConverter;
import org.jpos.iso.*;

public class IFA_LLHNUM_1987 extends ISOFieldPackager {
    private final Interpreter interpreter;
    private final Prefixer prefixer;

    public IFA_LLHNUM_1987(int len, String description) {
        super(len, description);
        interpreter = AsciiInterpreter.INSTANCE;
        prefixer = AsciiPrefixer.LL;
    }
    public IFA_LLHNUM_1987() {
        super();
        interpreter = AsciiInterpreter.INSTANCE;
        prefixer = AsciiPrefixer.LL;
    }
    public int unpack (ISOComponent c, byte[] b, int offset) throws ISOException {
        int len = Integer.parseInt(ISO8583CustomConverter.CustomVarLDecode(b, offset,2),16);

        if (len % 2 == 1) {
            len += 1;
        }

        c.setValue(interpreter.uninterpret(b, offset + prefixer.getPackedLength(), len));

        return prefixer.getPackedLength() + len;
    }


    public byte[] pack (ISOComponent c) throws ISOException {

        StringBuilder s = new StringBuilder(String.valueOf(c.getValue()));
        int len = s.length();
        if (s.substring(0,1).equals("0") && !s.substring(0,2).equals("00") ) {
            s.insert(0, ISO8583CustomConverter.CustomVarLEncode(len-1,2));
        }else {
            s.insert(0, ISO8583CustomConverter.CustomVarLEncode(len,2 ));
        }
        if (len > getLength() || len > 99)   // paranoia settings
            throw new ISOException(
                    "invalid len " + len + " packing IFA_LLHNUM_1987 field " + c.getKey()
            );

        len=len+2;
        byte[] b = new byte[ len ];
        interpreter.interpret(String.valueOf(s), b,0);
        return b;
    }


   public int getMaxPackedLength() {
        return 1 + ((getLength() + 1) >> 1);
    }
}

