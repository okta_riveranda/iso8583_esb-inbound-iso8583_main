package org.wso2.carbon.inbound.iso8583.listening.variable_length;

import org.jpos.iso.*;
import org.wso2.carbon.inbound.iso8583.listening.ISO8583CustomConverter;

public class IFH_ALPHANUM_1987 extends ISOFieldPackager {
    private final Interpreter interpreter;

    public IFH_ALPHANUM_1987() {
        super();
        interpreter = AsciiInterpreter.INSTANCE;
    }

    public IFH_ALPHANUM_1987(int len, String description) {
        super(len, description);
        interpreter = AsciiInterpreter.INSTANCE;
    }

    public int unpack(ISOComponent c, byte[] b, int offset) throws ISOException {
        int len = getLength() * 2;
        c.setValue(ISO8583CustomConverter.hexToAscii(interpreter.uninterpret(b, offset, len)));
        return len;
    }


    public byte[] pack(ISOComponent c) throws ISOException {
        String s = ISO8583CustomConverter.AsciiToHex(String.valueOf(c.getValue()));
        int len = s.length();
        if (len > getLength() * 2)   // paranoia settings
            throw new ISOException(
                    "invalid len " + len + " packing IFH_ALPHANUM_1987 field " + c.getKey()
            );

        byte[] b = new byte[len];
        interpreter.interpret(s, b, 0);
        return b;
    }

    public int getMaxPackedLength() {
        return 1 + ((getLength() + 1) >> 1);
    }


}
