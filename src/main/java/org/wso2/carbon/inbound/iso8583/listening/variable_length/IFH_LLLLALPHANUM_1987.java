
package org.wso2.carbon.inbound.iso8583.listening.variable_length;

import org.wso2.carbon.inbound.iso8583.listening.ISO8583CustomConverter;
import org.jpos.iso.*;

public class IFH_LLLLALPHANUM_1987 extends ISOFieldPackager {
    private final Interpreter interpreter;
    private final Prefixer prefixer;

    public IFH_LLLLALPHANUM_1987() {
        super();
        interpreter = AsciiInterpreter.INSTANCE;
        prefixer = AsciiPrefixer.LLLL;
    }

    public IFH_LLLLALPHANUM_1987(int len, String description) {
        super(len, description);
        interpreter = AsciiInterpreter.INSTANCE;
        prefixer = AsciiPrefixer.LLLL;
    }

    public int unpack (ISOComponent c, byte[] b, int offset) throws ISOException {
        int len = Integer.parseInt(ISO8583CustomConverter.CustomVarLDecode(b, offset,4), 16) * 2;
        c.setValue (ISO8583CustomConverter.hexToAscii(interpreter.uninterpret(b, offset + prefixer.getPackedLength(), len)));
        return prefixer.getPackedLength() + len;
    }

    public byte[] pack (ISOComponent c) throws ISOException {
        StringBuilder s =  new StringBuilder(ISO8583CustomConverter.AsciiToHex(String.valueOf(c.getValue())));
        int len = s.length();
        s.insert(0, ISO8583CustomConverter.CustomVarLEncode(len/2,4 ));
        if (len > getLength()*2 || len > 9999)   // paranoia settings
            throw new ISOException (
                    "invalid len "+len +" packing IFH_LLLLALPHANUM_1987 field " + c.getKey()
            );

        len=len+4;
        byte[] b = new byte[ len];
        interpreter.interpret(String.valueOf(s), b, 0);
        return b;
    }

    public int getMaxPackedLength() {
        return 1 + ((getLength()+1) >> 1);
    }


}
