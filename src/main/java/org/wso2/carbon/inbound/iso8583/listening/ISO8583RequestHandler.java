/*
 * Copyright (c) 2016, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.wso2.carbon.inbound.iso8583.listening;

import org.apache.log4j.Logger;
import org.apache.synapse.SynapseException;
import org.apache.synapse.inbound.InboundProcessorParams;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;

import javax.net.ssl.SSLException;
import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

/**
 * Class for handling the iso message request.
 */
public class ISO8583RequestHandler implements Runnable {
    private static final Logger log = Logger.getLogger(ISO8583RequestHandler.class);
    Properties properties;
    private Socket connection;
    private ISOPackager packager_Base;
    private String packager;
    private ISO8583MessageInject msgInject;
    private BufferedInputStream bufferedInputStream;
    private DataOutputStream outToClient;
    private Integer spacerByte;

    public ISO8583RequestHandler(Socket connection, InboundProcessorParams params) {
        try {
            this.connection = connection;
            properties = params.getProperties();
            this.packager = properties.getProperty("packager");
            this.spacerByte = Integer.valueOf(properties.getProperty("spacerByte"));
            this.packager_Base = ISO8583PackagerFactory.getPackager(this.packager, params);
            this.msgInject = new ISO8583MessageInject(params, connection);
            this.bufferedInputStream = new BufferedInputStream(this.connection.getInputStream());
            this.outToClient = new DataOutputStream(this.connection.getOutputStream());
        } catch (SSLException sslEx) {
            handleException("Connection failed! (non-SSL connection?)", sslEx);
        } catch (EOFException eofEx) {
            handleException("End of client data!", eofEx);
        } catch (IOException e) {
            handleException("Couldn't read the input streams!", e);
        }

    }

    /**
     * connect method for read the request from inputStreamReader and inject into sequence.
     */
    public void connect() throws IOException {
        boolean loop = true;
        int b = 0;
        int headerLength = 4;
        boolean isMultipleISOMessage = false;
        while (loop) {
            try {

                bufferedInputStream.mark(1);
                final int bytesRead = bufferedInputStream.read(new byte[1]);
                bufferedInputStream.reset();
                if (bytesRead != -1) {
                    log.debug("InputStream available, attempt to unpack message: " + connection.toString());

                    int x = this.spacerByte + headerLength;
                    int messageLength = bufferedInputStream.available();
                    byte[] message = new byte[messageLength];

                    // bufferedInputStream.skip(x);
                    bufferedInputStream.read(message);
                    // bufferedInputStream.read(message, 100, 100);
                    log.debug("ISO Request = " + new String(message, ISO8583Constant.ASCII_METHOD));

                    // check if message contains multiple ISO and if it is, extract message into multiple ISO messages
                    ArrayList<String> isoRequest = new ArrayList<>();
                    isoRequest = ISO8583CustomConverter.checkAndExtractMultipleISO(message);
                    log.debug("Request contains "+isoRequest.size()+" ISO Message(s)!");
                    // end

                    log.debug("Ready to unpack...");

                    if(!isoRequest.isEmpty())
                    {
                        int counter = 1;
                        boolean isMany = (isoRequest.size() > 1) ? true: false;

                        ISOMsg[] isoMessages = new ISOMsg[isoRequest.size()];
                        boolean isMessageRequest = true;
                        for (String req : isoRequest)
                        {
                            log.debug("Request "+counter+": "+req);
                            byte[] extractedMessage = req.getBytes();
                            ISOMsg isoMessage = unpackRequest(extractedMessage);
                            if (isoMessage.isRequest()) {
                                isoMessages[counter-1] = isoMessage;
                            }
                            else
                            {
                                log.debug("ISO message is invalid. Will echo back message to sender.");
                                isoMessage.recalcBitMap();
                                byte[] responseMessage = isoMessage.pack();
                                responseMessage = ISO8583CustomConverter.WrapMessage(this.spacerByte, responseMessage);
                                outToClient.write(responseMessage);
                                outToClient.flush();
                                log.debug("ISO Response Raw: " + new String(responseMessage, ISO8583Constant.ASCII_METHOD));
                                log.debug("ISO Response sent!");
                                isMessageRequest = false;
                            }
                            counter++;
                        }

                        if (isoMessages != null && isMessageRequest) {
                            msgInject.inject(isoMessages);
                        }

                    }
                    else
                    {
                        log.debug("There is no ISO request to be processed!");
                    }
                } else {
                    loop = false;
                    log.debug("InputStream not Available. Stop Waiting for message from: " + connection.toString());
                }
            } catch (EOFException e) {
                loop = false;
                log.debug("End of File reached. Stop Waiting for message from: " + connection.toString());
            } catch (IOException e1) {
                loop = false;
                log.debug("IO Exception. Stop Waiting for message from: " + connection.toString());
            } catch (Exception e2) {
                loop = false;
                log.debug("Exception. Stop Waiting for message from: " + connection.toString(), e2);
            }
        }
    }

    public void run() {
        try {
            connect();
        } catch (IOException e) {
            handleException("[IOException]Client may be disconnect the connection" + connection.toString(), e);
        } catch (Exception e1) {
            //catch (Exception e) {
            handleException("[Exception]Client may be disconnect the connection" + connection.toString(), e1);
        } finally {
            try {
                log.debug("Client closed." + connection.toString());
                bufferedInputStream.close();
                connection.close();
            } catch (IOException e) {
                log.debug("Couldn't close I/O streams" + connection.toString(), e);
            }
        }
    }

    /**
     * unpack the string iso message to obtain its fields.
     *
     * @param message String ISOMessage
     */
    private ISOMsg unpackRequest(byte[] message) {
        ISOMsg isoMsg = null;

        try {
            isoMsg = new ISOMsg();
            isoMsg.setPackager(this.packager_Base);

            String messageDumpHex;
            String messageDumpAscii;

            // special handling if the message is not in plain text
            if (ISO8583Constant.ISO8583_1993_VERSION.equals(this.packager)) {
                messageDumpAscii = new String(message, ISO8583Constant.ASCII_METHOD);
                message = ISO8583CustomConverter.bytesToHex(message).getBytes(ISO8583Constant.ASCII_METHOD);
                messageDumpHex = new String(message, ISO8583Constant.ASCII_METHOD);
                log.debug("[HEX] Raw Message before unpack: " + messageDumpHex);
                log.debug("[ASCII] Raw Message before unpack: " + messageDumpAscii);
            }
            isoMsg.unpack(message);
        } catch (ISOException e) {
            handleISOException(Arrays.toString(message), e);
        }
        return isoMsg;
    }

    /**
     * handle the ISOMessage which is not in the ISO Standard.
     *
     * @param message String ISOMessage
     */
    private void handleISOException(String message, ISOException e) {
        try {
            outToClient.writeBytes("Request ISO message is not in ISO Standard: " + message);
            handleException("Couldn't unpack the message since financial message is not in ISO Standard! " + connection.toString(), e);
        } catch (IOException e1) {
            handleException("OutputStream may be closed!", e1);
        }
    }

    /**
     * handle the Exception
     *
     * @param msg error message
     * @param e   an Exception
     */
    private void handleException(String msg, Exception e) {
        log.debug(msg, e);
        throw new SynapseException(msg);

    }
}
