/*
 * Copyright (c) 2016, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.wso2.carbon.inbound.iso8583.listening;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.synapse.SynapseException;
import org.apache.synapse.inbound.InboundProcessorParams;
import org.jpos.iso.ISOBasePackager;
import org.jpos.iso.ISOException;
import org.jpos.iso.packager.GenericPackager;

import java.util.Properties;

/**
 * class for get ISOPackager.
 */
public class ISO8583PackagerFactory {
    private static final Logger log = Logger.getLogger(ISO8583PackagerFactory.class);


    public static ISOBasePackager getPackager(String isoVersion, InboundProcessorParams params) {
        ISOBasePackager packager = null;
        int headerLength = 0;
        try {
            Properties properties = params.getProperties();
            ClassLoader loader = Thread.currentThread().getContextClassLoader();

            switch (isoVersion)
            {
                case ISO8583Constant.ISO8583_1987_VERSION:
                    packager = new GenericPackager(loader.getResourceAsStream(ISO8583Constant.ISO8583_1987_PACKAGER));
                    headerLength = (StringUtils.isNotEmpty(properties.getProperty(ISO8583Constant.INBOUND_HEADER_LENGTH))) ?
                            Integer.parseInt(properties.getProperty(ISO8583Constant.INBOUND_HEADER_LENGTH)) :
                            Integer.parseInt(properties.getProperty(ISO8583Constant.INBOUND_HEADER_LENGTH));
                    packager.setHeaderLength(headerLength);
                    break;
                case ISO8583Constant.ISO8583_1993_VERSION:
                    packager = new GenericPackager(loader.getResourceAsStream(ISO8583Constant.ISO8583_1993_PACKAGER));
                    headerLength = (StringUtils.isNotEmpty(properties.getProperty(ISO8583Constant.INBOUND_HEADER_LENGTH))) ?
                            Integer.parseInt(properties.getProperty(ISO8583Constant.INBOUND_HEADER_LENGTH)) :
                            Integer.parseInt(properties.getProperty(ISO8583Constant.INBOUND_HEADER_LENGTH));
                    packager.setHeaderLength(headerLength);
                    break;
                default:
                    log.error("ISO8583 version does not match!");
                    break;
            }

            if (log.isDebugEnabled()) {
                log.debug("ISO Message header length: " + headerLength);
            }

        } catch (NumberFormatException e) {
            handleException("One of the properties is invalid type", e);
        } catch (ISOException e) {
            handleException("Error while getting the ISOPackager", e);
        }
        return packager;
    }


    /**
     * handle the Exception
     *
     * @param msg error message
     * @param e   an Exception
     */
    private static void handleException(String msg, Exception e) {
        log.error(msg, e);
        throw new SynapseException(msg);
    }
}
