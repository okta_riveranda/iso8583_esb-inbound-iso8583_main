/*
 * Copyright (c) 2016, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.wso2.carbon.inbound.iso8583.listening;


import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.util.UIDGenerator;
import org.apache.axis2.context.MessageContext;
import org.apache.log4j.Logger;
import org.apache.synapse.core.SynapseEnvironment;
import org.apache.synapse.inbound.InboundEndpoint;
import org.apache.synapse.inbound.InboundProcessorParams;
import org.apache.synapse.mediators.base.SequenceMediator;
import org.apache.synapse.transport.customlogsetter.CustomLogSetter;
import org.jpos.iso.ISOMsg;
import org.wso2.carbon.base.MultitenantConstants;
import org.wso2.carbon.context.PrivilegedCarbonContext;
import org.gbg.cafs.predator.PredatorCompiler;
import org.gbg.cafs.predator.PredatorSendBack;

import java.net.Socket;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Properties;

/**
 * class for inject the iso xml messages into sequence.
 */
public class ISO8583MessageInject {
    private static final Logger log = Logger.getLogger(ISO8583MessageInject.class);
    private final String injectingSeq;
    private final String onErrorSeq;
    private final boolean sequential;
    private final SynapseEnvironment synapseEnvironment;
    private final InboundProcessorParams params;
    private final Socket connection;
    private final Properties properties;

    public ISO8583MessageInject(InboundProcessorParams params, Socket connection) {
        this.params = params;
        this.connection = connection;
        Properties properties = params.getProperties();
        this.injectingSeq = params.getInjectingSeq();
        this.onErrorSeq = params.getOnErrorSeq();
        this.sequential = Boolean.parseBoolean(properties.getProperty(ISO8583Constant.INBOUND_SEQUENTIAL));
        this.synapseEnvironment = params.getSynapseEnvironment();
        this.properties = this.params.getProperties();
    }
    
    public void inject(ISOMsg isoMessageReq, boolean isMany) {
        org.apache.synapse.MessageContext msgCtx = createMessageContext();
        msgCtx.setProperty("inbound.endpoint.name", params.getName());
        InboundEndpoint inboundEndpoint = msgCtx.getConfiguration().getInboundEndpoint(params.getName());
        CustomLogSetter.getInstance().setLogAppender(inboundEndpoint.getArtifactContainerName());
        msgCtx.setProperty(ISO8583Constant.ISO8583_INBOUND_MSG_ID, msgCtx.getMessageID());

        if (injectingSeq == null || injectingSeq.equals("")) {
            log.error("Sequence name not specified. Sequence : " + injectingSeq);
            return;
        }

        SequenceMediator seq = (SequenceMediator) synapseEnvironment.getSynapseConfiguration().getSequence(injectingSeq);
        try {

            String configPath = this.properties.getProperty(ISO8583Constant.ISO8583_CONFIGURATION_PATH);
            String replySenderList = PredatorCompiler.XMLGetElementName(configPath, ISO8583Constant.ReplySenderTag);
            String[] arrayReplySender = replySenderList.split("[,]", 0);

            List<String> MTIReplySender = Arrays.asList(arrayReplySender);

            // construct packed iso message
            if(MTIReplySender.contains(isoMessageReq.getMTI()))
            {
                PredatorSendBack PredatorReplySender = new PredatorSendBack(params);
                isoMessageReq = PredatorReplySender.sendBack(isoMessageReq);
            }

            PredatorCompiler predatorCompiler = new PredatorCompiler(params);
            ISOMsg isoMessageResponse =  (ISOMsg) isoMessageReq.clone();
            isoMessageResponse = predatorCompiler.ISOResponseComposer(isoMessageResponse);

            OMElement parentElement = messageBuilder(isoMessageResponse);
            msgCtx.getEnvelope().getBody().addChild(parentElement);
            ISO8583ReplySender replySender = new ISO8583ReplySender(connection, params);

            if (seq != null) {
                seq.setErrorHandler(onErrorSeq);
                if (log.isDebugEnabled()) {
                    log.debug("injecting message to sequence: " + injectingSeq);
                }
                synapseEnvironment.injectInbound(msgCtx, seq, sequential);
            } else {
                log.error("Sequence: " + injectingSeq + " not found");
            }

            replySender.sendBack(msgCtx, isMany);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Create the initial message context for ISO8583
     * create a synapse environment to send ISO8583 message to Inbound.
     */
    private org.apache.synapse.MessageContext createMessageContext() {
        org.apache.synapse.MessageContext msgCtx = synapseEnvironment.createMessageContext();
        MessageContext axis2MsgCtx =
                ((org.apache.synapse.core.axis2.Axis2MessageContext) msgCtx).getAxis2MessageContext();
        axis2MsgCtx.setServerSide(true);
        axis2MsgCtx.setMessageID(UIDGenerator.generateUID());
        msgCtx.setProperty(MessageContext.CLIENT_API_NON_BLOCKING, true);
        PrivilegedCarbonContext carbonContext = PrivilegedCarbonContext.getThreadLocalCarbonContext();
        axis2MsgCtx.setProperty(MultitenantConstants.TENANT_DOMAIN, carbonContext.getTenantDomain());
        return msgCtx;
    }

    /**
     * messageBuilder is used to build the xml iso messages.
     *
     * @param isomsg iso fields.
     */
    private OMElement messageBuilder(ISOMsg isomsg) {
        OMFactory OMfactory = OMAbstractFactory.getOMFactory();
        OMElement parentElement = OMfactory.createOMElement(ISO8583Constant.TAG_MSG, null);
        if (isomsg.getHeader() != null) {
            OMElement header = OMfactory.createOMElement(ISO8583Constant.HEADER, null);
            header.setText(Base64.getEncoder().encodeToString(isomsg.getHeader()));
            parentElement.addChild(header);
        }
        OMElement result = OMfactory.createOMElement(ISO8583Constant.TAG_DATA, null);

        for (int i = 0; i <= isomsg.getMaxField(); i++) {
            if (isomsg.hasField(i)) {
                String outputResult = isomsg.getString(i);
                OMElement messageElement = OMfactory.createOMElement(ISO8583Constant.TAG_FIELD, null);
                messageElement.addAttribute(OMfactory.createOMAttribute(ISO8583Constant.TAG_ID, null, String.valueOf(i)));
                messageElement.setText(outputResult);
                result.addChild(messageElement);
                parentElement.addChild(result);
            }

        }
        return parentElement;
    }

    public void inject(ISOMsg[] isoMessages)
    {

        org.apache.synapse.MessageContext[] msgsCtx = new org.apache.synapse.MessageContext[isoMessages.length];

        if (injectingSeq == null || injectingSeq.equals("")) {
            log.error("Sequence name not specified. Sequence : " + injectingSeq);
            return;
        }

        SequenceMediator seq = (SequenceMediator) synapseEnvironment.getSynapseConfiguration().getSequence(injectingSeq);
        try {

            String configPath = this.properties.getProperty(ISO8583Constant.ISO8583_CONFIGURATION_PATH);
            String replySenderList = PredatorCompiler.XMLGetElementName(configPath, ISO8583Constant.ReplySenderTag);
            String[] arrayReplySender = replySenderList.split("[,]", 0);

            List<String> MTIReplySender = Arrays.asList(arrayReplySender);

            // construct packed iso message
            boolean isProceed = true;
            int msgCtxCounter = 0;
            for (ISOMsg isoMessage: isoMessages)
            {
                if(!MTIReplySender.contains(isoMessage.getMTI()))
                {
                    isProceed = false;
                    break;
                }
                else
                {
                    org.apache.synapse.MessageContext msgCtx = createMessageContext();
                    msgCtx.setProperty("inbound.endpoint.name", params.getName());
                    InboundEndpoint inboundEndpoint = msgCtx.getConfiguration().getInboundEndpoint(params.getName());
                    CustomLogSetter.getInstance().setLogAppender(inboundEndpoint.getArtifactContainerName());
                    msgCtx.setProperty(ISO8583Constant.ISO8583_INBOUND_MSG_ID, msgCtx.getMessageID());
                    msgsCtx[msgCtxCounter] = msgCtx;
                    msgCtxCounter++;
                }
            }

            if(isProceed)
            {
                PredatorSendBack PredatorReplySender = new PredatorSendBack(params);
                isoMessages = PredatorReplySender.sendBack(isoMessages);
            }
            else
            {
                log.error("Cannot proceed due to incorrect MTI sent!");
            }

            for(int i = 0; i < isoMessages.length; i++)
            {
                PredatorCompiler predatorCompiler = new PredatorCompiler(params);
                ISOMsg isoMessageResponse =  (ISOMsg) isoMessages[i].clone();
                isoMessageResponse = predatorCompiler.ISOResponseComposer(isoMessageResponse);

                OMElement parentElement = messageBuilder(isoMessageResponse);
                msgsCtx[i].getEnvelope().getBody().addChild(parentElement);
                ISO8583ReplySender replySender = new ISO8583ReplySender(connection, params);

                if (seq != null) {
                    seq.setErrorHandler(onErrorSeq);
                    if (log.isDebugEnabled()) {
                        log.debug("injecting message to sequence: " + injectingSeq);
                    }
                    synapseEnvironment.injectInbound(msgsCtx[i], seq, sequential);
                } else {
                    log.error("Sequence: " + injectingSeq + " not found");
                }

                replySender.sendBack(msgsCtx);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
