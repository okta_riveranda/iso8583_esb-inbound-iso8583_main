package org.gbg.cafs.predator;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.synapse.SynapseException;
import org.apache.synapse.inbound.InboundProcessorParams;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.wso2.carbon.inbound.iso8583.listening.ISO8583Constant;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

public class PredatorSendBack {
    private static final Logger log = Logger.getLogger(PredatorSendBack.class.getName());
    private final InboundProcessorParams params;
    private final Properties properties;

    public PredatorSendBack(InboundProcessorParams params) {
        this.params = params;
        this.properties = this.params.getProperties();
    }

    public static CloseableHttpClient getCloseableHttpClient() {
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClients.custom().setHostnameVerifier(new AllowAllHostnameVerifier()).
                    setSslcontext(new SSLContextBuilder().loadTrustMaterial(null, (arg0, arg1) -> true).build()).build();
        } catch (KeyManagementException e) {
            log.error("KeyManagementException in creating http client instance", e);
        } catch (NoSuchAlgorithmException e) {
            log.error("NoSuchAlgorithmException in creating http client instance", e);
        } catch (KeyStoreException e) {
            log.error("KeyStoreException in creating http client instance", e);
        }
        log.debug("Predator Connection Initiated.");
        return httpClient;
    }

    private static void handleException(Exception e) {
        log.error("Predator sendBack:  Error", e);
        throw new SynapseException("Predator sendBack:  Error", e);
    }

    public ISOMsg sendBack(ISOMsg isomsg) throws ISOException {
        String responseCode;
        responseCode = PredatorConstant.PREDATOR_RESPONSE_CODES[2];
        try {

            PredatorCompiler predatorCompiler = new PredatorCompiler(this.params);
            String configPath = this.properties.getProperty(ISO8583Constant.ISO8583_CONFIGURATION_PATH);

            String responseString;
            String CDATA = PredatorCompiler.XMLBodyComposer(configPath, isomsg);
            String wsURL = PredatorCompiler.XMLGetElementName(configPath, "wsURL");
            String SOAPAction = PredatorCompiler.XMLGetElementName(configPath, "soapaction");
            String namespaceURI = PredatorCompiler.XMLGetElementName(configPath, "namespaceURI");
            String namespaceURIprefix = PredatorCompiler.XMLGetElementName(configPath, "namespaceURIprefix");
            ByteArrayOutputStream bout = new ByteArrayOutputStream();

            log.debug("Predator wsURL: "+wsURL);
            log.debug("Predator SOAPAction: "+SOAPAction);

            String xmlInput =
                    "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:" + namespaceURIprefix + "=\"" + namespaceURI + "\">" +
                            "<soapenv:Header/>" +
                            "<soapenv:Body>" +
                            "<" + namespaceURIprefix + ":ProcessTransaction>" +
                            "<!--Optional:-->" +
                            "<" + namespaceURIprefix + ":transactionData>" +
                            "<![CDATA[" + CDATA + "]]>" +
                            "</" + namespaceURIprefix + ":transactionData>" +
                            "</" + namespaceURIprefix + ":ProcessTransaction>" +
                            "</soapenv:Body>" +
                            "</soapenv:Envelope>";

            log.debug("Predator XML Request created: " + xmlInput);
            byte[] buffer = xmlInput.getBytes();
            bout.write(buffer);
            byte[] b = bout.toByteArray();

            // Set the appropriate HTTP Post parameters.
            // Prepare HTTP post
            HttpPost post = new HttpPost(wsURL);
            post.setHeader("Content-Type", "text/xml; charset=utf-8");
            post.setHeader("SOAPAction", SOAPAction);
            post.setEntity(new InputStreamEntity(new ByteArrayInputStream(b)));

            //Initiate a client connection to Predator Web service and execute POST.
            CloseableHttpClient httpClient = getCloseableHttpClient();
            CloseableHttpResponse res = httpClient.execute(post);
            log.debug("Predator XML Request sent!");

            //Read the response.
            if ((res.getStatusLine() != null) && (res.getStatusLine().getStatusCode() == 200))
            {
                responseString = EntityUtils.toString(res.getEntity());
                PredatorUtil obj = new PredatorUtil(responseString);
                String alert = obj.getXMLResponseField(PredatorConstant.PREDATOR_APPROVAL_PARAMETERS[0]);
                String ruleDecision = obj.getXMLResponseField(PredatorConstant.PREDATOR_APPROVAL_PARAMETERS[1]);
                responseCode = obj.getResponseCode(Boolean.parseBoolean(alert), ruleDecision);

                log.debug("Predator XML Response: " + responseString);
                log.debug("Predator XML Response (" + PredatorConstant.PREDATOR_APPROVAL_PARAMETERS[0] + "): " + alert);
                log.debug("Predator XML Response (" + PredatorConstant.PREDATOR_APPROVAL_PARAMETERS[1] + "): " + ruleDecision);
            }
            else
            {
                log.error("Unable to get a response from Predator Web Service. Response Code: " + res.getStatusLine().getStatusCode());
            }

            // replace the ISO response code
            log.debug("ISO8583 Response Code: " + responseCode);
            isomsg.set(ISO8583Constant.RESPONSE_CODE_FIELD_NUMBER, responseCode);

            // Release Predator Connection
            post.releaseConnection();
            log.debug("Predator connection released!");
            return isomsg;
        } catch (Exception e) {
            // Should there is an exception occurs, like Predator Web Service is down then it will still proceed to send out ISO Response to ITM
            log.error("Something has occured! Error: "+e.getMessage());
            isomsg.set(ISO8583Constant.RESPONSE_CODE_FIELD_NUMBER, responseCode);
            return isomsg;
        }
    }

    public ISOMsg[] sendBack(ISOMsg[] isoMessages) throws ISOException {
        String responseCode;
        responseCode = PredatorConstant.PREDATOR_RESPONSE_CODES[2];
        try {

            for(int i = 0; i < isoMessages.length; i++)
            {
                PredatorCompiler predatorCompiler = new PredatorCompiler(this.params);
                String configPath = this.properties.getProperty(ISO8583Constant.ISO8583_CONFIGURATION_PATH);

                String responseString;
                String CDATA = PredatorCompiler.XMLBodyComposer(configPath, isoMessages[i]);
                String wsURL = PredatorCompiler.XMLGetElementName(configPath, "wsURL");
                String SOAPAction = PredatorCompiler.XMLGetElementName(configPath, "soapaction");
                String namespaceURI = PredatorCompiler.XMLGetElementName(configPath, "namespaceURI");
                String namespaceURIprefix = PredatorCompiler.XMLGetElementName(configPath, "namespaceURIprefix");
                ByteArrayOutputStream bout = new ByteArrayOutputStream();

                log.debug("Predator wsURL: "+wsURL);
                log.debug("Predator SOAPAction: "+SOAPAction);

                String xmlInput =
                        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:" + namespaceURIprefix + "=\"" + namespaceURI + "\">" +
                                "<soapenv:Header/>" +
                                "<soapenv:Body>" +
                                "<" + namespaceURIprefix + ":ProcessTransaction>" +
                                "<!--Optional:-->" +
                                "<" + namespaceURIprefix + ":transactionData>" +
                                "<![CDATA[" + CDATA + "]]>" +
                                "</" + namespaceURIprefix + ":transactionData>" +
                                "</" + namespaceURIprefix + ":ProcessTransaction>" +
                                "</soapenv:Body>" +
                                "</soapenv:Envelope>";

                log.debug("Predator XML Request created: " + xmlInput);
                byte[] buffer = xmlInput.getBytes();
                bout.write(buffer);
                byte[] b = bout.toByteArray();

                // Set the appropriate HTTP Post parameters.
                // Prepare HTTP post
                HttpPost post = new HttpPost(wsURL);
                post.setHeader("Content-Type", "text/xml; charset=utf-8");
                post.setHeader("SOAPAction", SOAPAction);
                post.setEntity(new InputStreamEntity(new ByteArrayInputStream(b)));

                //Initiate a client connection to Predator Web service and execute POST.
                CloseableHttpClient httpClient = getCloseableHttpClient();
                CloseableHttpResponse res = httpClient.execute(post);
                log.debug("Predator XML Request sent!");

                //Read the response.
                if ((res.getStatusLine() != null) && (res.getStatusLine().getStatusCode() == 200))
                {
                    responseString = EntityUtils.toString(res.getEntity());
                    PredatorUtil obj = new PredatorUtil(responseString);
                    String alert = obj.getXMLResponseField(PredatorConstant.PREDATOR_APPROVAL_PARAMETERS[0]);
                    String ruleDecision = obj.getXMLResponseField(PredatorConstant.PREDATOR_APPROVAL_PARAMETERS[1]);
                    responseCode = obj.getResponseCode(Boolean.parseBoolean(alert), ruleDecision);

                    log.debug("Predator XML Response: " + responseString);
                    log.debug("Predator XML Response (" + PredatorConstant.PREDATOR_APPROVAL_PARAMETERS[0] + "): " + alert);
                    log.debug("Predator XML Response (" + PredatorConstant.PREDATOR_APPROVAL_PARAMETERS[1] + "): " + ruleDecision);
                }
                else
                {
                    log.error("Unable to get a response from Predator Web Service. Response Code: " + res.getStatusLine().getStatusCode());
                }

                // replace the ISO response code
                log.debug("ISO8583 Response Code: " + responseCode);
                isoMessages[i].set(ISO8583Constant.RESPONSE_CODE_FIELD_NUMBER, responseCode);

                // Release Predator Connection
                post.releaseConnection();
                log.debug("Predator connection released!");
            }
            return isoMessages;
        } catch (Exception e) {
            // Should there is an exception occurs, like Predator Web Service is down then it will still proceed to send out ISO Response to ITM
            log.error("Something has occured! Error: "+e.getMessage());
            for(int i = 0; i < isoMessages.length; i++)
            {
                isoMessages[i].set(ISO8583Constant.RESPONSE_CODE_FIELD_NUMBER, responseCode);
            }
            return isoMessages;
        }
    }
}
