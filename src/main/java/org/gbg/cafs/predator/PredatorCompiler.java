package org.gbg.cafs.predator;

import org.apache.log4j.Logger;
import org.apache.synapse.SynapseException;
import org.apache.synapse.inbound.InboundProcessorParams;
import org.jpos.iso.ISOMsg;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.wso2.carbon.inbound.iso8583.listening.ISO8583Constant;
import org.wso2.carbon.inbound.iso8583.listening.ISO8583CustomConverter;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;

public class PredatorCompiler {
    private static final Logger log = Logger.getLogger(PredatorCompiler.class.getName());
    private static String ISO8583ConfigurationPath = "";
    private final Properties properties;

    public PredatorCompiler(InboundProcessorParams params) {
        this.properties = params.getProperties();
        ISO8583ConfigurationPath = this.properties.getProperty(ISO8583Constant.ISO8583_CONFIGURATION_PATH);
    }

    public static String XMLGetElementName(String XPath, String targetElem) {
        String output = null;
        try {
            File inputFile = new File(XPath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            dbFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            Element el0 = (Element) (doc.getElementsByTagName(targetElem).item(0));
            NodeList nList0 = el0.getChildNodes();
            Node nNode = nList0.item(0);
            output = nNode.getTextContent();
        } catch (Exception e) {
            handleException("XMLGetElementName Error", e);
        }
        return output;
    }

    public static NodeList XMLGetElementName(String XPath, String targetElem, boolean returnChildList) {
        try {
            File inputFile = new File(XPath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            Element el0 = (Element) (doc.getElementsByTagName(targetElem).item(0));
            NodeList nList0 = el0.getChildNodes();
            if(returnChildList)
            {
                return nList0;
            }
        } catch (Exception e) {
            handleException("XMLGetElementName Error", e);
        }
        return null;
    }

    public static String XMLBodyComposer(String XPath, ISOMsg isomsg) { //,
        String output = null;
        String FieldName = null;
        try {

            String ISO8583RequestPath = PredatorCompiler.XMLGetElementName(XPath, ISO8583Constant.ISO8583Request);
            File inputFile = new File(ISO8583RequestPath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            dbFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("*");

            String tlvMessage = "";
            Hashtable extractedTLV = null;

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String isofield = "";
                    String offset = "";
                    String Datatype = "";
                    String Datatype2 = "";
                    String value = "";
                    String source = "";
                    String target = "";
                    int x = -99;
                    int y = -99;

                    String bypassFlag = "N";
                    String tlvFlag = "N";
                    String alternativeField = "N";

                    FieldName = eElement.getTagName();
                    if (!"".equals(eElement.getAttribute("isofield"))) {
                        isofield = eElement.getAttribute("isofield");
                        if (!isofield.equals("header")) {
                            String[] strIsofield = isofield.split(",");
                            if (strIsofield.length == 2) {
                                x = Integer.parseInt(strIsofield[0]);
                                y = Integer.parseInt(strIsofield[1]);
                            } else {
                                x = Integer.parseInt(strIsofield[0]);
                            }
                        }
                    }
                    if (!eElement.getAttribute("offset").equals("")) {
                        offset = eElement.getAttribute("offset");
                    }
                    if (!eElement.getAttribute("datatype").equals("")) {
                        Datatype = eElement.getAttribute("datatype");
                        eElement.removeAttribute("datatype");
                    }
                    if (!eElement.getAttribute("datatype2").equals("")) {
                        Datatype2 = eElement.getAttribute("datatype2");
                        eElement.removeAttribute("datatype2");
                    }
                    if (!eElement.getAttribute("bypass").equals("")) {
                        bypassFlag = eElement.getAttribute("bypass");
                        eElement.removeAttribute("bypass");
                    }
                    if (!eElement.getAttribute("tlvFlag").equals("")) {
                        tlvFlag = eElement.getAttribute("tlvFlag");
                        eElement.removeAttribute("tlvFlag");
                    }
                    if (!eElement.getAttribute("alternativeField").equals("")) {
                        alternativeField = eElement.getAttribute("alternativeField");
                        eElement.removeAttribute("alternativeField");
                    }
                    // check the bypass flag, if the bypass equals to Y then no need to set the value
                    if ("N".equals(bypassFlag)) {
                        if (!"".equals(isofield)) {
                            if (isofield.equals("header")) {
                                value = new String(isomsg.getHeader(), ISO8583Constant.ASCII_METHOD);
                                value = ISO8583CustomConverter.hexToAscii(value);
                                eElement.removeAttribute("isofield");
                            } else if (isomsg.hasField(x)) {
                                value = isomsg.getString(x);
                                eElement.removeAttribute("isofield");
                            } else if (isomsg.hasField(y) && !isomsg.hasField(x)) {
                                value = isomsg.getString(y);
                                if(y == PredatorConstant.BIT_TRACK2_DATA)
                                {
                                    int separatorIndex = value.indexOf("=");
                                    if(x == PredatorConstant.ALTERNATIVE_FIELDS[0])
                                    {
                                        value = PredatorUtil.extractTrack2DataB(value, PredatorConstant.BIT_TRACK2_DATA_MULTIPLE_FIELDS[0]);
                                    }
                                    else if(x == PredatorConstant.ALTERNATIVE_FIELDS[1])
                                    {
                                        value = value = PredatorUtil.extractTrack2DataB(value, PredatorConstant.BIT_TRACK2_DATA_MULTIPLE_FIELDS[1]);
                                    }
                                }
                                Datatype = Datatype2;
                                eElement.removeAttribute("isofield");
                            } else {
                                nNode.getParentNode().removeChild(nNode);
                                temp = temp - 1;
                                if (log.isDebugEnabled()) {
                                    log.debug("XMLBodyComposer removed field:" + FieldName + " due to ISO field " + x + " not available.");
                                }
                            }
                        }

                        if("Y".equals(tlvFlag))
                        {
                            tlvMessage = value;
                            // log.info("tlvMessage = "+tlvMessage);
                            PredatorUtil predatorUtil = new PredatorUtil();
                            extractedTLV = predatorUtil.initiateTLVExtraction(tlvMessage);
//                            extractedTLV.forEach(
//                                    (k,v) -> log.info("Key: "+k+" and Value: "+v)
//                            );
                        }

                        if (!offset.equals("")) {
                            String[] str = offset.split(",");
                            int[] arr = new int[2];
                            arr[0] = Integer.parseInt(str[0]);
                            arr[1] = Integer.parseInt(str[1]);

                            value = (ISO8583Constant.TLV == arr[0] && extractedTLV != null) ?
                                    (String)extractedTLV.get(str[1]) : value.substring(arr[0], arr[1]);

                            value = (value == null)? "" : value;
                            eElement.removeAttribute("offset");
                        }

                        if(!"".equals(value))
                        {
                            if(!"".equals(Datatype))
                            {
                                String[] str = Datatype.split(",");
                                Datatype = String.valueOf(str[0]);
                                source = String.valueOf(str[1]);
                                target = String.valueOf(str[2]);
                                value = getValueByDataType(value, isomsg, Datatype, source, target);
                            }
                            value = ("Y".equals(alternativeField)) ? PredatorUtil.extractTrack2DataB(value, PredatorConstant.BIT_TRACK2_DATA_MULTIPLE_FIELDS[2]) : value;
                            value = value.trim();
                            eElement.setTextContent(value);
                            if (log.isDebugEnabled()) {
                                log.debug("XMLBodyComposer set: " + value + " on field:" + FieldName);
                            }
                        }
                    }
                }
            }
            output = convertDocumentToString(doc);
        } catch (Exception e) {
            handleException("XMLBodyComposer Error on field :" + FieldName, e);
        }
        if (log.isDebugEnabled()) {
            log.debug("XMLBodyComposer end using template.");
        }
        return output;
    }

    public static String getValueByDataType(String value, ISOMsg isomsg, String dataType, String source, String target)
    {
        String result = "";

        try
        {
            switch (dataType) {
                case "LDecimal":
                    long integral = Long.parseLong(value.substring(Integer.parseInt(target)));
                    int counter = Integer.parseInt(value.substring(0, Integer.parseInt(target)));
                    BigDecimal unscaled = new BigDecimal(integral);
                    BigDecimal scaled = unscaled.scaleByPowerOfTen(-counter);
                    value = String.valueOf(scaled);
                    break;
                case "Formatter":
                    value = FormattertoValue(source, target, value);
                    break;
                case "DateTime":
                    Date date;
                    if (source.equals("MMddHHmmss")) {
                        String year = String.valueOf(Year.now().getValue());
                        if (isFutureDate(year.concat(value))) {
                            year = String.valueOf(Integer.parseInt(year) - 1);
                        }
                        value = year.concat(value);
                        source = "yyyyMMddHHmmss";
                    }
                    date = new SimpleDateFormat(source).parse(value);
                    value = new SimpleDateFormat(target).format(date);
                    break;
                case "RDecimal":
                    long integral2 = Long.parseLong(value);
                    int counter2 = Integer.parseInt(target);
                    BigDecimal unscaled2 = new BigDecimal(integral2);
                    BigDecimal scaled2 = unscaled2.scaleByPowerOfTen(-counter2);
                    value = String.valueOf(scaled2);
                    break;
                case "Currency":
                    String currCode = source;
                    if (isomsg.hasField(target)) {
                        currCode = isomsg.getString(target);
                    }
                    value = ISO8583CustomConverter.FormatCurrency(currCode, value);
                    break;
            }
        }
        catch (Exception exception)
        {
            handleException("getValueByDataType error: ", exception);
        }
        return result = value;
    }

    public static boolean isFutureDate(String pDateString) {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyyMMddHHmmss").parse(pDateString);
        } catch (ParseException e) {
            handleException("isFutureDate Error parsing Date: ", e);
        }
        return new Date().before(date);
    }

    public static String FormattertoValue(String type, String Formatter, String value) {

        try {
            switch (type) {
                case "float": {
                    float fValue = Float.parseFloat(value);
                    value = String.format(Formatter, fValue);
                    break;
                }
                case "string":
                case "": {
                    value = String.format(Formatter, value);
                    break;
                }
                case "int": {
                    int iValue = Integer.parseInt(value);
                    value = String.format(Formatter, iValue);
                    break;
                }
            }

        } catch (Exception e) {
            handleException("FormattertoValue Error:", e);
        }
        return value;
    }

    public static ISOMsg ISOResponseComposer(ISOMsg isomsg) {
        Integer FieldNumber = null;
        ISOMsg isoMessageResponse = (ISOMsg) isomsg.clone();

        // unset all fields from response
        int maxNo = isomsg.getMaxField();
        for (int i = 2; i <= maxNo; i++) {
            isoMessageResponse.unset(i);
        }

        try {
            isoMessageResponse.setResponseMTI();
            String XPath = PredatorCompiler.XMLGetElementName(ISO8583ConfigurationPath, ISO8583Constant.ISO8583Response);

            if (log.isDebugEnabled()) {
                log.debug("ISOResponseComposer start using template: " + XPath);
            }

            File inputFile = new File(XPath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            dbFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            NodeList channels = doc.getElementsByTagName(ISO8583Constant.ISO8583ISOResponseTag);
            for (int i = 0; i < channels.getLength(); i++)
            {
                Node node = channels.item(i);
                if (Node.ELEMENT_NODE == node.getNodeType())
                {
                    Element element = (Element) node;
                    String mti = element.getAttribute("mti");

                    if (mti != null && mti.equals(isoMessageResponse.getMTI()))
                    {
                        NodeList financialResponse = element.getChildNodes();
                        for (int j = 0; j < financialResponse.getLength(); j++)
                        {
                            Node responseNode = financialResponse.item(j);
                            if(Node.ELEMENT_NODE == responseNode.getNodeType())
                            {
                                Element eElement = (Element) responseNode;
                                String attr = "";
                                String defValue = "";
                                FieldNumber = Integer.valueOf(eElement.getTagName().replaceAll("field", ""));
                                if (!eElement.getAttribute("attr").equals(""))
                                {
                                    attr = eElement.getAttribute("attr");
                                }
                                if ("ME".equals(attr)) {
                                    if (!isomsg.hasField(FieldNumber)) {
                                        defValue = responseNode.getTextContent();
                                        isoMessageResponse.set(FieldNumber, defValue);
                                    } else {
                                        isoMessageResponse.set(FieldNumber, String.valueOf(isomsg.getValue(FieldNumber)));
                                    }
                                }
                                else if ("CE".equals(attr)) {
                                    if (isomsg.hasField(FieldNumber)) {
                                        isoMessageResponse.set(FieldNumber, String.valueOf(isomsg.getValue(FieldNumber)));
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
            }
            isoMessageResponse.recalcBitMap();
        } catch (Exception e) {
            handleException("ISOResponseComposer Error on field :" + FieldNumber, e);
        }
        if (log.isDebugEnabled()) {
            log.debug("ISOResponseComposer end using template.");
        }
        return isoMessageResponse;
    }

    private static String convertDocumentToString(Document doc) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        String output = null;
        try {
            transformer = tf.newTransformer();
            // below code to remove XML declaration
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            output = writer.getBuffer().toString().replaceAll("[\n\r]", "");

        } catch (TransformerException e) {
            handleException("convertDocumentToString Error", e);
        }
        return output;
    }

    private static void handleException(String msg, Exception e) {
        log.error(msg, e);
        throw new SynapseException(msg, e);
    }
}


