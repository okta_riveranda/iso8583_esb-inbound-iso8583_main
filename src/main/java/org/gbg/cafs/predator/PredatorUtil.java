package org.gbg.cafs.predator;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.synapse.SynapseException;
import org.jpos.iso.ISOMsg;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

public class PredatorUtil {

    private static final Logger log = Logger.getLogger(PredatorUtil.class.getName());

    Document doc;

    public PredatorUtil()
    {

    }

    /**
     *
     * @param predatorResponse: this is entire Predator XML Response
     */
    public PredatorUtil(String predatorResponse)
    {
        try
        {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(predatorResponse));
            doc = builder.parse(src);
        }
        catch(Exception e)
        {
            log.error("Error upon extracting the field!", e);
        }
    }

    /**
     *
     * @param fieldName: field name that will be extracted
     * @return
     */
    public String getXMLResponseField(String fieldName)
    {
        return (doc != null) ? doc.getElementsByTagName(fieldName).item(0).getTextContent() : "";
    }

    /**
     *
     * @param alert is the first parameter to determine what is the response code that will be returned back to ITM
     * @param ruleDecision is the second parameter to determine what is the response code that will be returned back to ITM
     *              logic:
     *              if <Alert>false</Alert> OR <RuleDecision></RuleDecision> is empty
     *              then it is Approved (00) else Rejected by Predator (01)
     * @return
     */
    public String getResponseCode(boolean alert, String ruleDecision)
    {
        return (!Boolean.valueOf(alert) || StringUtils.isEmpty(ruleDecision)) ?
                PredatorConstant.PREDATOR_RESPONSE_CODES[0]:PredatorConstant.PREDATOR_RESPONSE_CODES[1];
    }

    public Hashtable<String, String> initiateTLVExtraction(String tlvMessage)
    {
        Hashtable<String, String> extractedTLV = new Hashtable<String, String>();
        String fieldNo = tlvMessage.substring(0,2);
        String fieldLength = tlvMessage.substring(2,4);
        tlvMessage = tlvMessage.substring(fieldNo.length()+fieldLength.length());
        String fieldValue = tlvMessage.substring(0, Integer.parseInt(fieldLength));
        tlvMessage = tlvMessage.substring(fieldValue.length());
        ArrayList list = new ArrayList<String>(100);
        list.add(0, fieldNo);
        list.add(1, fieldLength);
        list.add(2, fieldValue);
        list.add(3, tlvMessage);
        while(list != null)
        {
            extractedTLV.put(list.get(0).toString(), list.get(2).toString());
            list = extractTLV(list);
        }
        return extractedTLV;
    }

    public ArrayList extractTLV(ArrayList list)
    {
        if(!StringUtils.isEmpty(list.get(3).toString()))
        {
            String fieldNo = list.get(3).toString().substring(0,2);
            String fieldLength = list.get(3).toString().substring(2,4);
            String fieldValue = list.get(3).toString().substring(4, 4 + Integer.parseInt(fieldLength));
            String tlvMessage = list.get(3).toString().substring(4 + Integer.parseInt(fieldLength));

            list.set(0, fieldNo);
            list.set(1, fieldLength);
            list.set(2, fieldValue);
            list.set(3, tlvMessage);

            return list;
        }
        else
        {
            return null;
        }
    }

    /**
     *
     * @param track2Data this is track 2 data value from bit 35 (DE-35)
     * @param fieldName field name desired to extract
     * @return
     */
    public static String extractTrack2DataB(String track2Data, String fieldName)
    {
        String result = "";
        int maxIndex = track2Data.length() - 1;
        int separatorIndex = track2Data.indexOf("=");

        // PAN
        if(PredatorConstant.BIT_TRACK2_DATA_MULTIPLE_FIELDS[0].equals(fieldName))
        {
            result = (separatorIndex == -1) ? track2Data.substring(0, maxIndex) : track2Data.substring(0, separatorIndex);
        }
        // Card Expiry
        else if(PredatorConstant.BIT_TRACK2_DATA_MULTIPLE_FIELDS[1].equals(fieldName))
        {
            result = (separatorIndex + 4 <= maxIndex && separatorIndex != -1) ?
                    track2Data.substring(separatorIndex+1, separatorIndex+5) : "";
        }
        // Service Code
        else
        {
            result = (separatorIndex + 7 <= maxIndex && separatorIndex != -1) ?
                    track2Data.substring(separatorIndex+5, separatorIndex+8) : "";
        }
        return result;
    }
}
