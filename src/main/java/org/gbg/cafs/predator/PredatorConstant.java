package org.gbg.cafs.predator;

public class PredatorConstant {
    // As of 31/03/2021, there are only 2 parameters from Predator XML Response to determine Predator Approval
    public static final String[] PREDATOR_APPROVAL_PARAMETERS = {"Alert", "RuleDecision"};
    // 00 = Approved; 01 = Rejected by Predator; 99 = Any errors
    public static final String[] PREDATOR_RESPONSE_CODES = {"00", "01", "99"};
    public static final int[] ALTERNATIVE_FIELDS = {2,14};
    public static final int BIT_TRACK2_DATA = 35;
    public static final String[] BIT_TRACK2_DATA_MULTIPLE_FIELDS = {"PAN", "CardExp", "ServiceCode"};
    public static final String PREDATOR_CHANNEL_ONLINE = "C05";
}
